const fs = require('fs');
const {ipcRenderer, remote} = require('electron');
const {dialog} = remote;
const imagesInput = document.getElementById('user-images');
const form = document.querySelector('form');
const saveProjectBtn = document.getElementById('save-project');
const loadProjectBtn = document.getElementById('load-project');
let images = [];

document.addEventListener('click', e => {
  const isRemoveBtn = e.target.classList.contains('btn-remove');

  if (isRemoveBtn) {
    removeImage(e.target.dataset.id);
  }
});

imagesInput.addEventListener('change', e => {
  const newImages = imagesInput.files;
  images = images.concat([].slice.call(newImages).map(newImage => ({path: newImage.path, id: getId()})));
  renderImagesList(images);
});

form.addEventListener('submit', e => {
  e.preventDefault();
  dialog.showSaveDialog(filename => {
    if (!filename) {
      console.log("You didn't save the file");
      return;
    }

    ipcRenderer.send('create-pdf', {images, filename});
  });
	e.target.reset();
});

saveProjectBtn.addEventListener('click', e => {
  dialog.showSaveDialog(fileName => {
    if (!fileName) {
      console.log("You didn't save the file");
      return;
    }

    fs.writeFile(`${fileName}.mm`, JSON.stringify(images), err => {
      if(err){
        alert("An error occurred creating the file "+ err.message)
      }

      alert("The file has been successfully saved");
    });
  });
});

loadProjectBtn.addEventListener('click', e => {
  dialog.showOpenDialog(fileNames => {
    if (!fileNames){
      console.log("You didn't choose any files");
      return;
    }

    fs.readFile(fileNames[0], 'utf8', (err, data) => {
      if (err) {
        alert("An error ocurred reading the file " + err.message)
      }

      images = JSON.parse(data);
      renderImagesList(images);
    });
  });
});

function removeImage(imageId) {
  images = images.filter(image => image.id !== imageId);
  renderImagesList(images);
}

function renderImagesList(images) {
  const imagesList = document.querySelector('ul');

  imagesList.innerHTML = images.map(
    image => `<li>
        <button class="btn-remove mui-btn mui-btn--xsmall mui-btn--danger" data-id="${image.id}">remove</button>
        ${image.path}
        <img width="200" src="${image.path}">
    </li>`
  ).join('');
}

function getId() {
  function s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
      .toString(16)
      .substring(1);
  }
  return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
    s4() + '-' + s4() + s4() + s4();
}