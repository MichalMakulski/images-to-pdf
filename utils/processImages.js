const fs = require('fs');
const PdfPrinter = require('pdfmake/src/printer');
const printer = new PdfPrinter();

function processImages(e, data) {
  const a4 = [840, 592];
  const content = data.images.map(img => ({image: img.path, fit: a4}));
  const dd = {
    pageOrientation:'landscape',
    pageMargins: [0,0,0,0],
    content: content
  };
  const pdfDoc = printer.createPdfKitDocument(dd);

  pdfDoc
    .pipe(fs.createWriteStream(`${data.filename}.pdf`))
    .on('finish',() => {
      //success
    });
  pdfDoc.end();
}

module.exports = processImages;